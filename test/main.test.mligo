#import "../src/main.mligo" "MyContract"

let init_storage =
{
    metadata = (Big_map.empty : (string, bytes) big_map);
    participants = (Set.literal [
      ("tz1Ps66mRUJmQsQnfNjc1bzXtjbWzHjXttkS": address);
      ("tz1burnburnburnburnburnburnburjAYjjX": address)
      ] : address set);
   locked_tez = (Map.empty : (address, tez) map);
   secrets = (Map.empty : (address, chest) map);
   decoded_payloads = (Map.empty : (address, bytes) map);
   result_nat = (None : nat option);
   last_seed = 3268854739249n;
   max = 1000n;
   min = 100n}

let test =
    let (taddr, _, _) = Test.originate MyContract.main init_storage 0tez in
    let contr = Test.to_contract taddr in
    let addr =  Tezos.address contr in
    ()
