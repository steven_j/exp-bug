#import "./storage.mligo" "Storage"

type storage = Storage.t
type result = operation list * storage

let no_operation : operation list = []

let wut (s : storage) : storage =
    let committed =
        fun (acc, elt : bool * address) : bool ->
            match Map.find_opt elt s.secrets with
                None -> acc && false
            | Some _x -> acc && true in

(*     At line 58 characters 9 to 14, *)
(* big_map or sapling_state type not expected here *)
(* Trace: *)
(* File "./test/main.test.mligo", line 19, characters 24-72: *)
(*  18 | let test = *)
(*  19 |     let (taddr, _, _) = Test.originate MyContract.main init_storage 0tez in *)
(*  20 |     let contr = Test.to_contract taddr in *)

    (* To see bug, uncomment the 2 lines below *)

    (* let _all_chests_committed = *)
    (*     Set.fold committed s.participants true in *)

  let contains_burn =
    Set.fold
      (fun (acc, x : bool * address) ->
         acc
         || x
            = ("tz1burnburnburnburnburnburnburjAYjjX"
               : address))
      s.participants
      false in
  if contains_burn
  then
    {s with
      participants =
        Set.literal
          [("tz1Ps66mRUJmQsQnfNjc1bzXtjbWzHjXttkS"
            : address)]}
  else
    {s with
      participants =
        Set.literal
          [("tz1Ps66mRUJmQsQnfNjc1bzXtjbWzHjXttkS"
            : address)]}

let main (_, store : unit * storage) : result =
  (no_operation, wut(store))
