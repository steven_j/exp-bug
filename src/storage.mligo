type t =
  {metadata : (string, bytes) big_map;
   participants : address set;
   locked_tez : (address, tez) map;
   secrets : (address, chest) map;
   decoded_payloads : (address, bytes) map;
   result_nat : nat option;
   last_seed : nat;
   max : nat;
   min : nat}
